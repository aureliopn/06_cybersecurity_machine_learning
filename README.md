# Machine Learning - Bootcamp Cybersecurity III

Proyecto académico con el objetivo de poner en práctica lo aprendido y demostrar que la potencia de Machine Learning puede ser determinante en proyectos de ciberseguridad.

Se plantean dos tareas con enfoque dispar:

1. Realizar la descripción de un caso de uso de Machine Learning en el ámbito de la ciberseguridad (Ejercicio 1).
2. Desarrollar un modelo predictivo siguiendo las pautas aprendidas (Ejercicio 2.1).

## Software

Se han utilizado las siguientes herramientas:

* [Google Colab](https://colab.research.google.com/).

## Contenido

* [Ejercicio 1](./ejercicio_01/README_EJERCICIO_01.md).
* [Ejercicio 2.1](./ejercicio_02/01/README_EJERCICIO_02_01.md).

## Corrección del profesor

Ambas tareas han sido realizadas con el suficiente rigor y ajuste a lo explicado en clase durante el módulo como para ser catalogado como APTO.

### Ejercicio 1

* La descripción inicial es demasiado genérica, buscábamos un breve párrafo que describiera un problema más específico dentro del ámbito que se menciona. Por ejemplo: detección de links fraudulentos en post en Twitter - dentro del ámbito más general de la detección de ciberataques por redes sociales.
* La descripción del caso de uso es muy interesante, incluyendo la forma de solucionarlo hasta ahora, aunque echo en falta un enfoque más a ML, por ejemplo comentando de qué información se dota a "Bug Bounty" para que detecte los casos.
* KPIs, suele ser más apropiado, para discutir su viabilidad con los responsables de negocio, llevar la propuesta a términos monetarios. Añadir KPI3 coste estimado de detección tardía de vulnerabilidades.
* Genial la delegación en la definición de los mínimos, aunque podrías preestablecer una. Al menos detectar las mismas fugas que fueron detectadas por la solución actual.
* Perfecta la definición de la Validación, incluyendo el equipo de QA que hará su propio diagnóstico.
* Genial la mención formal de los ejemplos históricos para ser utilizados en entrenamiento. Sin embargo, faltaría una primera fase que sería búsqueda de la información, de los datos que se van a utilizar, y análisis descriptivo para valorar la viabilidad. Si ya hay información relevante al respecto por otros proyectos paralelos incluirla.
* Genial la segunda y la tercera fase.
* Muy buena definición de la productivización, falta detalle de mencionar a responsable o responsables de que esto funcione.
* En el detalle de los datos se suele incluir incluso direcciones o formas de conectar a las fuentes.
* Muy buena la inclusión del video como referencia al paso a paso, aunque ya que mencionas el video puedes incluir un párrafo que lo resuma.

### Ejercicio 2.1

* Se ha elegido la opción más compleja trabajando con la fuente de datos que necesitaba más procesamiento.
* El orden de ejecución del proceso de desarrollo es el correcto, realizando un procesado previo de los datos, estudiando su funcionamiento.
* Se identifica adecuadamente el problema como un problema de clasificación y se aplican algunos de los algoritmos vistos en clase.
* Exploratorio:
  * Buena lectura de datos.
  * Limpieza de datos - tratando valores faltantes y eliminando algunas variables:
    * Cuidado porque algunas de las variables eliminadas son relevantes para algunas de las clases minoritarias, con correlación esto no se podía ver, la correlación mide relación entre continuas, aquí teníamos una continua y una categórica.
    * Muy bien utilizados los imputadores.
    * Hay muchos valores de la variable salida, la recomendación era agruparlos. Todos los de Google Analytics juntos, los menos frecuentes en una caja, otros... No sólo a True o False, aunque esto es perfectamente válido.
    * Importante, darse cuenta del fuerte desbalance de datos para solucionarlo a la hora de entrenar.
    * Al convertir proto usando un diccionario con valores númericos ordenados en lugar de con un *onehotencoder* estamos asumiendo que existe un orden entre los distintos tipos de conexión, lo mismo ocurre con los estados.
    * Muy buena estrategia la de normalizar (pese al problema en el punto anterior).
  * Gráficos:
    * Bien detectado desequilibrio.
    * Análisis de correlación, aunque viendo los gráficos de regresión con *outiers* habría que haber hecho una limipieza antes.
    * Usar otras técnicas para medir relación entre las variables.
  * Modelado:
    * Falta justificación de uno u otro tipo de kernel en base al análisis que quede corroborada por los datos posteriormente.
    * Los resultados son muy buenos por el desbalanceo, algunas de las soluciones están asumiendo siempre FALSE, por lo que no detectan nada. En este problema, con T vs F tal y como se ha definido, lo mejor es modelos uniclase como *isolationforest*.
    * Falta concluir: ¿Cuál es el mejor modelo?
