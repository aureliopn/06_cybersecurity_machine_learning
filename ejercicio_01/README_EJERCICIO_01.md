# Machine Learning - Ejercicio 1 - Caso de Uso de Machine Learning

## Enunciado

A lo largo del módulo se han ido estudiando distintos métodos y algoritmos útiles en la resolución de distintos problemas de ciberseguridad a partir de datos. También hemos aprendido la metodología de desarrollo y hemos estudiado las distintas claves que permiten evaluar el éxito del proyecto.

Como expertos en ciberseguridad vuestro rol será fundamental a la hora de establecer las bases, evaluar y realizar el seguimiento de distintas soluciones de Machine Learning que se puedan plantear como solución ante un problema de seguridad. Vuestro trabajo en muchos de estos proyectos será definir adecuadamente este documento y aseguraros de que se van cumpliendo las especificaciones haciendo uso del conocimiento adquirido en este módulo.

Por lo tanto el objetivo de la tarea es plantear la definición de un caso de uso que se pudiera presentar como proyecto potencial a realizar con ML en una compañía
siguiendo las instrucciones descritas en el documento [Conceptualizacion_CasosDeUso_BigData_v20.pdf](./Conceptualizacion_CasosDeUso_BigData_v20.pdf).

## Solución

### Descripción del caso de uso

En este informe se describe la idea de utilizar automatismos en la producción de software para la detección temprana de potenciales problemas de seguridad.

#### ¿Cuál es el problema?

Contexto:

* Existe un producto web que consta de dos partes: API y UI.
* Ambas partes son susceptibles de tener vulnerabilidades de seguridad.
* En el ciclo de producción del software no existen procesos dedicados al descubrimiento de problemas de seguridad del producto.
* Es muy probable que se introduzcan vulnerabilidades de seguridad en el producto con cada nueva versión que se libera en producción.

Problema:

* Las vulnerabilidades de seguridad del producto son casi siempre descubiertas en producción.

Impacto:

* Un problema de seguridad del producto afectaría potencialmente a todos los clientes que lo utilizan.
* En función de la gravedad de la vulnerabilidad habría mayor o menor:
  * Publicidad negativa sobre el producto y sobre la empresa.
  * Repercusión económica por los efectos negativos de la explotación de la vulnerabilidad.

#### ¿Cómo se está afrontando ahora?

Actualmente el producto está suscrito a un programa de "*Bug Bounty*" para la detección de vulnerabilidades en producción.

(*Poner aquí informe con el número de vulnerabilidades descubiertas en los últims años gracias al programa de "Bug Bounty"*).

#### ¿Acción que buscamos poder hacer par solucionar el problema?

Se pretenden utilizar mecanismos automáticos para la detección de las vulnerabilidades de seguridad en la construcción del producto antes de su paso a producción. Estas acciones no son una solución al 100% del problema, se busca reducir la probabilidad de liberar versiones del producto con vulnerabilidades.

**No se pretende reemplazar la suscripción al programa de "*Bug Bounty*" que detecta las vulnerabilidades del producto en producción.**

#### KPIs - Indicadores de negocio

Atributos KPIs:

1. Número de vulnerabilidades descubiertas con los mecanismos automáticos en un año.
2. Número de vulnerabilidades descubiertas con el programa de "Bug Bounty" en un año.

La efectividad de la solución se puede medir considerando como resultados positivos:

* Un valor alto del KPI 1.
* Un valor bajo del KPI 2.

#### ¿Cuáles son los mínimos que se esperan de este caso de uso?

Estos valores los debe establecer una combinación de uno o varios de los siguientes actores:

* Equipo de negocio.
* *Product Owner*.

Ejemplo:

* El valor del KPI 1 debe ser mucho mayor que el KPI 2, o deben valer los dos "0" (cero).

#### Validación: ¿Qué criterio se va a usar para decidir si la solución es aceptable?

Los resultados de los mecanismos automáticos pueden tener las siguientes deficiencias:

* No detección de vulnerabilidades existentes en el producto (positivos no detectados).
* Detección de falsos positivos.

Efectos de las deficiencias:

* Una baja detección de las vulnerabilidades existentes en el producto puede acarrear no alcanzar los **mínimos que se esperan de este caso de uso**.
* Una alta tasa de falsos positivos reduciría la agilidad del proceso de producción del software y la frecuencia de la liberación de nuevas versiones en producción.

El citerio de aceptación de la solución lo debe establecer una combinación de uno o varios de los siguientes actores:

* Equipo de QA.
* Equipo de desarrollo.

El criterio de aceptación estará en función del sobrecoste que supone la detección de un falso positivo estimado por los actores involucrados.

Ejemplo:

* Debe detectar el 60% de los casos.
* Debe acertar el 90% de las detecciones.

Con cada vulnerabilidad detectada por el *modelo ML* el equipo de desarrollo y/o el equipo de QA diagnosticará si es o no un falso positivo.

#### Experimentación: ¿Cómo vamos a corroborar el funcionamiento?

* En la primera etapa realizar una validación del *modelo ML*:
  * Partir de la versión del producto en producción en una rama de desarrollo dedicada a este caso de uso.
  * Utilizar un entorno no productivo para la rama de desarrollo creada.
  * Añadir regresiones en en la rama de desarrollo que se correspondan con vulnerabilidades encontradas en el pasado con el programa "*Bug Bounty*".
  * Probar la detección por parte del *modelo ML* de las vulnerabilidades conocidas introducidas (sin falsos positivos).
* En la segunda etapa:
  * Añadir una tarea en los *pipelines* del ciclo de desarrollo del software que no sea bloqueante, sólo informativa, y que utilice el *modelo ML* desarrollado en el punto anterior.
  * Estudiar los resultados de la tarea en el ciclo real de producción del software, y realizar los ajustes necesarios en el *modelo ML*.
* En la tercera etapa:
  * Establecer como bloqueante la detección de una vulnerabilidad en los *pipelines* del ciclo de desarrollo del software.

En el caso de detectar problemas en los mecanismos automáticos introducidos que repercutan en una pérdida de agilidad en la producción del software, siempre es posible volver a la segunda etapa para realizar las correcciones necesarias.

En el caso de que existan vulnerabilidades detectadas desde el programa de "*Bug Bounty*", mejorar el modelo *modelo ML* con la nueva información obtenida.

#### Productivización: ¿Qué salida debe tener la solución que se desarrolle?

Al ser un mecanismo automático introducido en los *pipelines* del ciclo de desarrollo del software, la salida será:

* En caso de éxito (**no** se han encontrado vulnerabilidades): ejecutar la siguiente tarea del *pipeline* (la propia de este tipo de tareas).
* En caso de fallo (**sí** se han detectado vulnerabilidades):
  * Generar informe (aunque sea texto plano) sobre la vulnerabilidad encontrada.
  * Crear una *issue* en el sistema de gestión del proyecto para trabajar en la corrección necesaria del software.
  * Establecer la ejecución del *pipeline* con el error detectado (el propio sistema realizará las notificaciones oportunas).

### Equipo de trabajo

#### Identificación de personas colaboradoras

Es necearia una o varias personas con alguno de los siguientes perfiles:

* Minero de datos (prepara los datos para su explotación).
* Analista de datos.
* Científico de datos.
* Experto en ciberseguridad.
* Experto en *Machine Learning*.

Trabajando en equipo con los siguientes actores:

* Equipo de negocio.
* *Product Owner*.
* Equipo de QA.
* Equipo de desarrollo.

### Detalle del caso de uso

(*Se puede utilizar como apoyo el documento [Machine_Learning_Checklist.pdf](./Machine_Learning_Checklist.pdf)*).

#### Detalle funcional

Es conveniente:

* Aprovechar la información disponible como equipo de *Blue Team*.
* Estudiar las dependencias de componentes de terceros para facilitar la detección de vulnerabilidades potenciales (existe acceso al código fuente del producto).
* Estudiar si conviene utilizar soluciones externas, por ejemplo:
  * [Snyk](https://snyk.io/plans/).
  * [Kiuwan](https://www.kiuwan.com/).
  * [Archery](https://www.archerysec.com/).
* Capturar todo el tráfico de entrada/salida de las peticiones sobre el API de las pruebas de integración del producto para construir el *modelo ML*.

#### Identificación de orígenes de datos

Tipos de datos a utilizar:

* Código fuente del producto.
* Lista de dependencias de componentes de terceros.
* *Endpoints* del API.
* Tráfico de las pruebas de integración.
* Código de las pruebas de aceptación existentes con [Selenium](https://www.selenium.dev/).

Según los orígenes de datos y herramientas que se deseen utilizar es posible que este caso de uso se pueda dividir en otros casos de uso.

### Desarrollo del caso de uso

#### Puntos intermedios o seguimiento

Es posible que todo esto, orientado por la naturaliza del "Ejercicio" de *utilizar ML orientado a ciberseguridad*, se pueda separar en otros casos de uso de *Machine Learning* orientados simplemente a *Testing*:

[![A Practical Example for Using AI to Improve your UI and API Testing](http://img.youtube.com/vi/68mEgr0vO64/0.jpg)](https://youtu.be/68mEgr0vO64)

#### Aporte esperado por el Big Data

El uso de mecanismos automáticos con *Machine Learning* debe:

* Mejorar la experiencia de usuario.
* Mejorar la calidad y seguridad del producto.
* Añadir innovación en el proceso de [DevOps](https://en.wikipedia.org/wiki/DevOps), o [DevSecOps](https://snyk.io/series/devsecops/).
* Reconducir la utilización de los recursos personales de los equipos de trabajo:
  * Dedicar menos tiempo a realizar pruebas manuales sobre el producto.
  * Dedicar más tiempo a la investigación y mejora de los *Modelos ML* o *IA* utilizados en las fases de pruebas del producto.
* Aumentar la automatización de los procesos de desarrollo del software.
