# Machine Learning - Ejercicio 2.1 - Desarrollar Modelo Predictivo

## Enunciado

El objetivo será identificar el tipo de tráfico que está habiendo en base a las variables recogidas y otras se puedan generar.

El conjunto de datos utilizado será el que se puede encontrar en el siguiente link, donde además se puede ver su estructura.

Se evaluará el resultado pero también el trabajo realizado en base a la metodología Data Science.

[Web oficial de la base de datos](https://mcfp.weebly.com/the-ctu-13-dataset-a-labeled-dataset-with-botnet-normal-and-background-traffic.html).

De todos los escenarios posibles se ha determinado trabajar con el **escenario 7** por contener menos datos y se recomienda utilizar los ficheros csv que se adjuntan. [Fuente final de datos: Escenario 7](https://mcfp.felk.cvut.cz/publicDatasets/CTU-Malware-Capture-Botnet-48/).

Hay varios ficheros, aunque de todos se puede extraer información valiosa, se recomienda utilizar simplemente:

* [capture20110816-2.binetflow.2.format](https://mcfp.felk.cvut.cz/publicDatasets/CTU-Malware-Capture-Botnet-48/capture20110816-2.binetflow.2format)
Fichero csv que contiene información sobre las distintas conexiones con una variable objetivo final en la columna flow que determina el background.
* [Enlace a drive](https://drive.google.com/drive/folders/1CLxUKaWomYzdg68wVhq-y0gCbt2F0HD6?usp=sharing).

## Solución

Se ha desarrollado la solución en el siguiente Notebook: [traffic_flow.ipynb](./traffic_flow.ipynb).

Posteriormente, se ha mejorado el modelo con una nueva versión: [traffic_flow_v2.ipynb](./traffic_flow_v2.ipynb).
